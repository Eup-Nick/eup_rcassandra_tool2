package DB_Operate_RRedis;

import java.util.Map;

import DB_Operate_Object.TTLEnumType;

public interface IDB_Operate_Redis {
	public void setnx(String key, String value) throws Exception;
	public void setnx(String key, TTLEnumType ttlEnumType, String value) throws Exception;

	
	public void set(String key, String value) throws Exception;

	public void set(String key, TTLEnumType ttlEnumType, String value) throws Exception;

	
	public String get(String key) throws Exception;

	
	public Map<String, String> mget(String... key) throws Exception;


	public boolean exists(String key) throws Exception;

	/// End: type-string ///

	/// Start: type-map ///

	public void hset(String key, String field, TTLEnumType ttlEnumType, String value) throws Exception;

	public void hmset(String key, TTLEnumType ttlEnumType, Map<String, String> value) throws Exception;

	public String hget(String key, String field) throws Exception;

	public Map<String, String> hgetall(String key) throws Exception;

	/// End: type-map ///

	public void dispose();

}
