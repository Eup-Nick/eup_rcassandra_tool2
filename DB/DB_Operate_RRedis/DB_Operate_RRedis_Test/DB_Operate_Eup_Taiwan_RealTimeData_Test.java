package DB_Operate_RRedis.DB_Operate_RRedis_Test;

import DB_Operate_RRedis.DB_Operate_Redis_ClusterBase_SyncPool;
import DB_Operate_RRedis.IDB_Operate_Redis;

public class DB_Operate_Eup_Taiwan_RealTimeData_Test {

	private static String useName = "eup_redis_lognow_test";
	private static IDB_Operate_Redis db_Operate = null;
	private static final Object syncRoot = new Object();

	public static IDB_Operate_Redis getInstance() {
		if (db_Operate == null) {
			synchronized (syncRoot) {
				if (db_Operate == null) {
					db_Operate = createDbOperate();
				}
			}
		}
		return db_Operate;
	}

	private static IDB_Operate_Redis createDbOperate() {
		return new DB_Operate_Redis_ClusterBase_SyncPool() {
			@Override
			protected String getUseName() {
				return useName;
			}
		};
//		if (International_Country.getElementByCountryTag(DB_Connect_Info.getInstance().getNationCode()).equals(International_Country.TW)) {
//		} else {
//			return new DB_Operate_Redis_SingleBase_SyncPool() {
//				@Override
//				protected String getUseName() {
//					return useName;
//				}
//			};
//		}
	}
}
