package DB_Operate_RRedis;

import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import DB_Connect_Infomation.DB_Connect_Info;
import DB_Connect_Infomation.DB_Infomation;
import DB_Operate_Object.TTLEnumType;
import io.lettuce.core.KeyValue;
import io.lettuce.core.RedisURI;
import io.lettuce.core.TimeoutOptions;
import io.lettuce.core.cluster.ClusterClientOptions;
import io.lettuce.core.cluster.ClusterClientOptions.Builder;
import io.lettuce.core.cluster.ClusterTopologyRefreshOptions;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import io.lettuce.core.cluster.api.sync.RedisAdvancedClusterCommands;
import io.lettuce.core.resource.DefaultClientResources;
import io.lettuce.core.support.ConnectionPoolSupport;

public abstract class DB_Operate_Redis_ClusterBase_SyncPool implements IDB_Operate_Redis {

	private static final Object synced = new Object();
	private static DefaultClientResources sharedResource = DefaultClientResources.create();
	private RedisClusterClient client;
	private GenericObjectPool<StatefulRedisClusterConnection<String, String>> pool;
	private int maxIdlePool = 8, minIdlePool = 4, maxTotalPool = 8;
	private int timeoutSec = 5;

	protected abstract String getUseName();

	public DB_Operate_Redis_ClusterBase_SyncPool() {
		// Redis Key Naming:
		// - lognow:unicode:eup
		// - lognow/unicode/eup
		// - crm:web-account:eup:web-password:eup
		// - crm:web.account:eup:web.password:eup
	}

	private void initPool(Boolean reInit) throws Exception {
		if (reInit) {
			if (pool != null) {
				pool.close();
				pool = null;
			}
			if (client != null) {
				client.shutdown();
				client = null;
			}
		}
		if (pool == null || client == null) {
			synchronized (synced) {
				if (pool == null || client == null) {
					if (pool != null) {
						pool.close();
						pool = null;
					}
					if (client != null) {
						client.shutdown();
						client = null;
					}

					DB_Infomation info = DB_Connect_Info.getInstance().getDBInfo(getUseName());
					String[] ips = info.ipAddress.split(",");
					String[] ports = info.port.split(",");
					List<RedisURI> redisURIs = new ArrayList<>();
					for (int i = 0; i < ips.length; i++) {
						RedisURI redisURI = RedisURI.Builder.redis(ips[i]).withPort(Integer.valueOf(ports[i])).withPassword(info.password)
								.withTimeout(Duration.ofSeconds(15)).build();
						redisURIs.add(redisURI);
					}

					ClusterTopologyRefreshOptions topologyRefreshOptions = ClusterTopologyRefreshOptions.builder()
							.enablePeriodicRefresh(Duration.ofMinutes(15)).enableAllAdaptiveRefreshTriggers()
							.adaptiveRefreshTriggersTimeout(Duration.ofSeconds(30)).build();
					Builder builder = ClusterClientOptions.builder();
					builder = builder.cancelCommandsOnReconnectFailure(true).requestQueueSize(256)
							.timeoutOptions(TimeoutOptions.enabled(Duration.ofSeconds(timeoutSec)));
					builder = builder.topologyRefreshOptions(topologyRefreshOptions);

					client = RedisClusterClient.create(sharedResource, redisURIs);
					client.setOptions(builder.build());

					GenericObjectPoolConfig<StatefulRedisClusterConnection<String, String>> poolConfig = new GenericObjectPoolConfig<StatefulRedisClusterConnection<String, String>>();
					poolConfig.setMaxTotal(maxTotalPool);
					poolConfig.setMaxIdle(maxIdlePool);
					poolConfig.setMinIdle(minIdlePool);
					pool = ConnectionPoolSupport.createGenericObjectPool(() -> {
						return client.connect();
					}, poolConfig);
				}
			}
		}
	}

	/// Begin: type-string ///

	@Override
	public void setnx(String key, String value) throws Exception {
		setnx(key, TTLEnumType.FOREVER, value);
	}

	@Override
	public void setnx(String key, TTLEnumType ttlEnumType, String value) throws Exception {
		initPool(false);
		StatefulRedisClusterConnection<String, String> connection = pool.borrowObject();
		try {
			RedisAdvancedClusterCommands<String, String> commands = connection.sync();
			Boolean isExec = commands.setnx(key, value);
			if (isExec && !ttlEnumType.equals(TTLEnumType.FOREVER))
				commands.expire(key, ttlEnumType.getIntValue());
		} finally {
			closeConnection(connection);
		}
	}

	@Override
	public void set(String key, String value) throws Exception {
		set(key, TTLEnumType.FOREVER, value);
	}

	@Override
	public void set(String key, TTLEnumType ttlEnumType, String value) throws Exception {
		initPool(false);
		StatefulRedisClusterConnection<String, String> connection = pool.borrowObject();
		try {
			RedisAdvancedClusterCommands<String, String> commands = connection.sync();
			if (!ttlEnumType.equals(TTLEnumType.FOREVER))
				commands.setex(key, ttlEnumType.getIntValue(), value);
			else
				commands.set(key, value);
		} finally {
			closeConnection(connection);
		}
	}

	@Override
	public String get(String key) throws Exception {
		initPool(false);
		StatefulRedisClusterConnection<String, String> connection = pool.borrowObject();
		try {
			RedisAdvancedClusterCommands<String, String> commands = connection.sync();
			return commands.get(key);
		} finally {
			closeConnection(connection);
		}
	}

	@Override
	public Map<String, String> mget(String... keys) throws Exception {
		initPool(false);
		StatefulRedisClusterConnection<String, String> connection = pool.borrowObject();
		try {
			RedisAdvancedClusterCommands<String, String> commands = connection.sync();
			List<KeyValue<String, String>> kvList = commands.mget(keys);

			Map<String, String> reuslt = new HashMap<>();
			for (KeyValue<String, String> kv : kvList) {
				reuslt.put(kv.getKey(), kv.getValue());
			}
			return reuslt;
		} finally {
			closeConnection(connection);
		}
	}

	@Override
	public boolean exists(String key) throws Exception {
		initPool(false);
		StatefulRedisClusterConnection<String, String> connection = pool.borrowObject();
		try {
			RedisAdvancedClusterCommands<String, String> commands = connection.sync();
			Long future = commands.exists(key);
			return future == 1;
		} finally {
			closeConnection(connection);
		}
	}

	/// End: type-string ///

	/// Start: type-map ///

	@Override
	public void hset(String key, String field, TTLEnumType ttlEnumType, String value) throws Exception {
		initPool(false);
		StatefulRedisClusterConnection<String, String> connection = pool.borrowObject();
		try {
			RedisAdvancedClusterCommands<String, String> commands = connection.sync();
			if (!ttlEnumType.equals(TTLEnumType.FOREVER)) {
				commands.hset(key, field, value);
				commands.expire(key, ttlEnumType.getIntValue());
			} else
				commands.hset(key, field, value);
		} finally {
			closeConnection(connection);
		}
	}

	public void hmset(String key, TTLEnumType ttlEnumType, Map<String, String> value) throws Exception {
		initPool(false);
		StatefulRedisClusterConnection<String, String> connection = pool.borrowObject();
		try {
			RedisAdvancedClusterCommands<String, String> commands = connection.sync();
			if (!ttlEnumType.equals(TTLEnumType.FOREVER)) {
				commands.hmset(key, value);
				commands.expire(key, ttlEnumType.getIntValue());
			} else
				commands.hmset(key, value);
		} finally {
			closeConnection(connection);
		}
	}

	public String hget(String key, String field) throws Exception {
		initPool(false);
		StatefulRedisClusterConnection<String, String> connection = pool.borrowObject();
		try {
			RedisAdvancedClusterCommands<String, String> commands = connection.sync();
			return commands.hget(key, field);
		} finally {
			closeConnection(connection);
		}
	}

	public Map<String, String> hgetall(String key) throws Exception {
		initPool(false);
		StatefulRedisClusterConnection<String, String> connection = pool.borrowObject();
		try {
			RedisAdvancedClusterCommands<String, String> commands = connection.sync();
			Map<String, String> kvList = commands.hgetall(key);
			return kvList;
		} finally {
			closeConnection(connection);
		}
	}

	/// End: type-map ///

	private void closeConnection(StatefulRedisClusterConnection<String, String> connection) {
		connection.close();
		// pool.returnObject(connection);
	}

	@SuppressWarnings("unused")
	private void addressTransaltor() {
		// MappingSocketAddressResolver resolver =
		// MappingSocketAddressResolver.create(DnsResolvers.UNRESOLVED, hostAndPort -> {
		// System.out.println(hostAndPort.getHostText() + hostAndPort.getPort());
		// if (hostAndPort.getHostText().equals("192.168.10.81") && hostAndPort.getPort() == 7001)
		// return HostAndPort.of("203.69.250.198", 7001);
		// else if (hostAndPort.getHostText().equals("192.168.10.82") && hostAndPort.getPort() == 7001)
		// return HostAndPort.of("203.69.250.198", 7003);
		// else if (hostAndPort.getHostText().equals("192.168.10.83") && hostAndPort.getPort() == 7001)
		// return HostAndPort.of("203.69.250.198", 7005);
		// return hostAndPort;
		// });
		// ClientResources clientResources =
		// ClientResources.builder().socketAddressResolver(resolver).build();

		// client = RedisClusterClient.create(clientResources, redisURIs);
	}

	public void dispose() {
		if (pool != null) {
			pool.close();
			pool = null;
		}
		if (client != null) {
			client.shutdown();
			client = null;
		}
	}
}
