package CassandraIPMapping_Information;

import java.io.File;
import java.net.InetSocketAddress;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import OS_Checker.OS_Type;
import OS_Checker.OS_Validator;

public class CassandraIp_Mapping_Info {

	private static CassandraIp_Mapping_Info connect_Info;
	private static final Object sync = new Object();
	private Map<String, Map<InetSocketAddress, InetSocketAddress>> clusterToCassandraIpMapping = new HashMap<>();

	public static CassandraIp_Mapping_Info getInstance() {
		if (connect_Info == null)
			synchronized (sync) {
				if (connect_Info == null)
					connect_Info = new CassandraIp_Mapping_Info();
			}

		return connect_Info;
	}

	private CassandraIp_Mapping_Info() {
		readXML();
	}

	private void readXML() {
		try {
			File fXmlFile = null;
			if (OS_Validator.getOSType() == OS_Type.Linux)
				fXmlFile = new File("/opt/tomcat/conf/CassandraIp_Mapping.XML");
			else
				fXmlFile = new File("C:\\Config\\CassandraIp_Mapping.XML");
			if (!fXmlFile.exists())
				return;

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			readCassandraIpMapping(doc);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void readCassandraIpMapping(Document doc) {
		NodeList mappingList = doc.getElementsByTagName("CassandraIpMapping");
		for (int j = 0; j < mappingList.getLength(); j++) {
			Element mappingElement = (Element) mappingList.item(j);
			String clusterName = mappingElement.getElementsByTagName("CassandraClusterName").item(0).getTextContent();
			Map<InetSocketAddress, InetSocketAddress> cassandraIpMapping = new HashMap<>();
			NodeList ipList = mappingElement.getElementsByTagName("CassandraIp");
			for (int i = 0; i < ipList.getLength(); i++) {
				Node node = ipList.item(i);
				if (node.getNodeType() == Node.ELEMENT_NODE) {
					Element element = (Element) node;
					String privateIp = element.getElementsByTagName("PrivateIp").item(0).getTextContent();
					int privatePort = Integer.parseInt(element.getElementsByTagName("PrivatePort").item(0).getTextContent());
					String publicIp = element.getElementsByTagName("PublicIp").item(0).getTextContent();
					int publicPort = Integer.parseInt(element.getElementsByTagName("PublicPort").item(0).getTextContent());
					cassandraIpMapping.put(new InetSocketAddress(privateIp, privatePort), new InetSocketAddress(publicIp, publicPort));
				}
			}
			clusterToCassandraIpMapping.put(clusterName, cassandraIpMapping);
		}
	}

	public Map<InetSocketAddress, InetSocketAddress> getCassandraIpMapping(String clusterName) {
		Map<InetSocketAddress, InetSocketAddress> map = clusterToCassandraIpMapping.get(clusterName);
		if (map == null)
			return new HashMap<>();
		return new HashMap<>(map);
	}
}
