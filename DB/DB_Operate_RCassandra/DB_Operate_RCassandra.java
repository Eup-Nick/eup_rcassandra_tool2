package DB_Operate_RCassandra;

import java.math.BigDecimal;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.datastax.driver.core.BatchStatement;
import com.datastax.driver.core.BoundStatement;
import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ColumnDefinitions;
import com.datastax.driver.core.ConsistencyLevel;
import com.datastax.driver.core.DataType;
import com.datastax.driver.core.HostDistance;
import com.datastax.driver.core.PoolingOptions;
import com.datastax.driver.core.PreparedStatement;
import com.datastax.driver.core.ProtocolOptions.Compression;
import com.datastax.driver.core.QueryOptions;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.SocketOptions;
import com.datastax.driver.core.TupleType;
import com.datastax.driver.core.TupleValue;
import com.datastax.driver.core.UDTValue;
import com.datastax.driver.core.UserType;
import com.datastax.driver.core.policies.AddressTranslator;
import com.datastax.driver.core.policies.DCAwareRoundRobinPolicy;
import com.datastax.driver.core.policies.ExponentialReconnectionPolicy;
import com.datastax.driver.core.policies.TokenAwarePolicy;
import com.google.common.base.Strings;

import CassandraIPMapping_Information.CassandraIp_Mapping_Info;
import DB_Connect_Infomation.DB_Connect_Info;
import DB_Connect_Infomation.DB_Infomation;
import DB_Operate_Object.TTLEnumType;

public abstract class DB_Operate_RCassandra {

	private static Object synced = new Object();
	private Cluster cluster = null;
	private Session session = null;
	private Map<String, PreparedStatement> map = new ConcurrentHashMap<>();
	private String dbName;

	protected abstract String getUseName();

	public DB_Operate_RCassandra() throws Exception {
		connectDB();
	}

	private void init() throws Exception {
		if (cluster == null || session == null || session.isClosed() || cluster.isClosed()) {
			synchronized (synced) {
				if (cluster != null && !cluster.isClosed()) {
					cluster.close();
					cluster = null;
				}
				if (session != null && !session.isClosed()) {
					session.close();
					session = null;
				}
				connectDB();
			}
		}
	}

	private void connectDB() throws Exception {
		DB_Infomation info = DB_Connect_Info.getInstance().getDBInfo(getUseName());
		// 當只有一個DC時，設置LOCAL即可；反之多需設置REMOTE
		PoolingOptions poolingOptions = new PoolingOptions();
		poolingOptions.setConnectionsPerHost(HostDistance.LOCAL, 4, 10); // DC中的每個機器至少有幾個連接，最多有幾個連接(此預設為min:1;max:1)
		poolingOptions.setConnectionsPerHost(HostDistance.REMOTE, 2, 4);
		poolingOptions.setMaxRequestsPerConnection(HostDistance.LOCAL, 1024); // 每個連接的最大請求數量(LOCAL預設為1024)
		poolingOptions.setMaxRequestsPerConnection(HostDistance.REMOTE, 256); // 每個連接的最大請求數量(REMOTE預設為256)
		poolingOptions.setHeartbeatIntervalSeconds(60); // 設置HeartBeat的發送時間(此預設為30)
		poolingOptions.setIdleTimeoutSeconds(120); // idle連線的超時則被回收。回收時間需要10s。(此預設為120)
		poolingOptions.setMaxQueueSize(256); // 拋出沒有可用的連線異常前，可以容納的request數(此預設為256)
		QueryOptions queryOptions = new QueryOptions();
		queryOptions.setFetchSize(2000); // 查詢結果fetch的行數(此預設為5000)
		queryOptions.setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM); // Query等級。(此預設為LOCAL_ONE)

		SocketOptions socketOptions = new SocketOptions();
		socketOptions.setReadTimeoutMillis(12 * 100000); // query讀取超時。(此預設12秒)
		socketOptions.setConnectTimeoutMillis(5 * 1000); // 等待TCP連線時間。(此預設5秒)

		String[] ips = info.ipAddress.split(",");
		String[] ports = info.port.split(",");
		List<InetSocketAddress> inetSocketAddresses = new ArrayList<>();
		for (int i = 0; i < ips.length; i++) {
			if (i <= ports.length - 1)
				inetSocketAddresses.add(new InetSocketAddress(ips[i], Integer.parseInt(ports[i])));
			else
				inetSocketAddresses.add(new InetSocketAddress(ips[i], 9042));
		}
		// LoadBalancingPolicy：預設使用[TokenAwarePolicy]Token決定查詢的node，若無設定Token(where條件中只能有PatitionKey)則使用[DCAwareRoundRobinPolicy]循環取得node
		// ReconnectionPolicy：預設使用[ExponentialReconnectionPolicy]，單位秒，以1秒開始，連線間最大延遲10分鐘，最多嘗試64次。指數地增加每個重新連線之間的延遲時間，例如1,2,4,8...
		// RetryPolicy：預設使用[DefaultRetryPolicy]，當readtimeout時，(得到的副本>=所需的副本)且(沒有取得資料)才進行重試，並最多執行一次。
		cluster = Cluster.builder().addContactPointsWithPorts(inetSocketAddresses).withCredentials(info.loginID, info.password)
				.withPoolingOptions(poolingOptions).withAddressTranslator(new EupOnlineAddressTranslater(info.clusterName))
				.withQueryOptions(queryOptions).withCompression(Compression.SNAPPY)
				.withLoadBalancingPolicy(new TokenAwarePolicy(DCAwareRoundRobinPolicy.builder().build()))
				.withReconnectionPolicy(new ExponentialReconnectionPolicy(1000, 60 * 1000)).withoutJMXReporting().withSocketOptions(socketOptions)
				.build(); // .withSSL()
		session = cluster.connect(info.dbName);
		dbName = info.dbName;
		map = new ConcurrentHashMap<>();
	}

	public UserType getUserType(String nameType) {
		return cluster.getMetadata().getKeyspace(getUseName()).getUserType(nameType);
	}

	public TupleType getTupleType(List<DataType> dataTypes) {
		return cluster.getMetadata().newTupleType(dataTypes);
	}

	/**
	 * 查詢(SELECT)
	 */
	public Map<String, List<Object>> executeQuery(String sqlquery, List<Object> paramList, int retryAmount, int retryWaitSec) throws Exception {
		return executeQuery(sqlquery, paramList, null, retryAmount, retryWaitSec);
	}

	/**
	 * 查詢(SELECT)
	 */
	public Map<String, List<Object>> executeQuery(String sqlquery, List<Object> paramList, Integer readTimeoutMills, int retryAmount,
			int retryWaitSec) throws Exception {
		Map<String, List<Object>> result = new HashMap<>();
		boolean isSuccess = false;
		while (!isSuccess && retryAmount > 0) {
			try {
				result = subExecuteQuery(sqlquery, paramList, readTimeoutMills, retryAmount, retryWaitSec);
				isSuccess = true;
			} catch (Exception e) {
				retryAmount--;
				Thread.sleep(retryWaitSec * 1000);
			}
		}
		return result;
	}

	private Map<String, List<Object>> subExecuteQuery(String sqlquery, List<Object> paramList, Integer readTimeoutMills, int retryAmount,
			int retryWaitSec) throws Exception {
		Map<String, List<Object>> result = new HashMap<>();
		init();
		BoundStatement boundStatement = getBoundStatement(sqlquery, paramList);
		if (readTimeoutMills != null)
			boundStatement.setReadTimeoutMillis(readTimeoutMills.intValue());
		boundStatement.setIdempotent(true);
		ResultSet resultSet = session.execute(boundStatement); // 同步執行方法
		if (resultSet != null) {
			ColumnDefinitions definitions = resultSet.getColumnDefinitions();
			for (ColumnDefinitions.Definition definition : definitions) {
				if (!result.containsKey(definition.getName()))
					result.put(definition.getName(), new ArrayList<>());
			}

			for (Row row : resultSet) {
				if (resultSet.getAvailableWithoutFetching() == 500 && !resultSet.isFullyFetched())
					resultSet.fetchMoreResults();

				for (ColumnDefinitions.Definition definition : definitions) {
					if (definition.getType().getName().name().equals(DataType.timestamp().getName().name())) {
						Object o = row.getObject(definition.getName());
						result.get(definition.getName()).add(o.toString());
					} else {
						Object o = row.getObject(definition.getName());
						result.get(definition.getName()).add(o);
					}
				}
			}
		} else
			throw new Exception("查詢資料回傳NULL，請檢查SQL 語法或該方法 SQL：" + sqlquery);
		return result;
	}

	/**
	 * 查詢(非SELECT)(同步)
	 * 
	 * @return 是否成功
	 */
	public boolean executeNonQuery(String sqlquery, List<Object> paramList) throws Exception {
		init();
		BoundStatement boundStatement = getBoundStatement(sqlquery, paramList);
		ResultSet resultSet = session.execute(boundStatement); // 同步執行方法
		return resultSet.wasApplied();
	}

	/**
	 * 查詢(非SELECT)(非同步)
	 */
	public void executeAsyncNonQuery(String sqlquery, List<Object> paramList) throws Exception {
		init();
		BoundStatement boundStatement = getBoundStatement(sqlquery, paramList);
		session.executeAsync(boundStatement);
	}

	/**
	 * 批次查詢(非SELECT)(非同步)
	 */
	public void executeBatchAsyncNonQuery(String sqlquery, List<List<Object>> paramList) throws Exception {
		init();
		BatchStatement batchStatement = new BatchStatement();
		for (List<Object> params : paramList) {
			batchStatement.add(getBoundStatement(sqlquery, params));
		}
		session.executeAsync(batchStatement);
	}

	/**
	 * 設定參數的型別
	 */
	private BoundStatement getBoundStatement(String sqlquery, List<Object> paramList) throws SQLException {
		BoundStatement boundStatement = null;
		PreparedStatement pStatement;
		synchronized (map) {
			pStatement = map.get(sqlquery);
			if (pStatement == null) {
				pStatement = session.prepare(sqlquery);
				map.put(sqlquery, pStatement);
			}
		}
		boundStatement = pStatement.bind();

		int index = 0;
		for (Object sql_Parameter : paramList) {
			if (sql_Parameter != null) {
				Class<? extends Object> type = sql_Parameter.getClass();
				if (type == Integer.class)
					boundStatement.setInt(index, Integer.parseInt(sql_Parameter.toString()));
				else if (type == Long.class)
					boundStatement.setLong(index, Long.parseLong(sql_Parameter.toString()));
				else if (type == String.class)
					boundStatement.setString(index, sql_Parameter.toString());
				else if (type == Double.class)
					boundStatement.setDouble(index, Double.parseDouble(sql_Parameter.toString()));
				else if (type == Float.class)
					boundStatement.setFloat(index, Float.parseFloat(sql_Parameter.toString()));
				else if (type == BigDecimal.class)
					boundStatement.setDecimal(index, (BigDecimal) sql_Parameter);
				else if (type == Boolean.class)
					boundStatement.setBool(index, Boolean.parseBoolean(sql_Parameter.toString()));
				else if (type == Date.class)
					boundStatement.setTimestamp(index, (Date) sql_Parameter);
				else if (List.class.isAssignableFrom(type))
					boundStatement.setList(index, (List<?>) sql_Parameter);
				else if (Map.class.isAssignableFrom(type))
					boundStatement.setMap(index, (Map<?, ?>) sql_Parameter);
				else if (Set.class.isAssignableFrom(type))
					boundStatement.setSet(index, (Set<?>) sql_Parameter);
				else if (type == UDTValue.class)
					boundStatement.setUDTValue(index, (UDTValue) sql_Parameter);
				else if (type == TupleValue.class)
					boundStatement.setTupleValue(index, (TupleValue) sql_Parameter);
				else if (ByteBuffer.class.isAssignableFrom(type))
					boundStatement.setBytes(index, (ByteBuffer) sql_Parameter);
			} else
				boundStatement.setToNull(index);
			index++;
		}
		return boundStatement;
	}

	/**
	 * [適用於INSERT]<br/>
	 * 查詢(非SELECT)(同步)，設定為Idempotent=true且會忽略NULL值(不更新也不新增)<br/>
	 * PS. Idempotent=true。當連線斷開，會重試此命令，故需確認此資料不是更新語法
	 * 
	 * @return 是否成功
	 */
	public boolean executeNonQueryByIgnoreNull(String sqlQuery, List<Object> paramList, TTLEnumType ttl) throws Exception {
		init();
		if (!Strings.isNullOrEmpty(ttl.getValue()))
			sqlQuery += " USING TTL " + ttl.getValue();
		BoundStatement boundStatement = getBoundStatementByIgnoreNull(sqlQuery, paramList);
		boundStatement.setIdempotent(true);
		ResultSet resultSet = session.execute(boundStatement); // 同步執行方法
		return resultSet.wasApplied();
	}

	/**
	 * [適用於INSERT]<br/>
	 * 查詢(非SELECT)(非同步)，設定為Idempotent=true且會忽略NULL值(不更新也不新增)<br/>
	 * PS. Idempotent=true。當連線斷開，會重試此命令，故需確認此資料不是更新語法
	 */
	public void executeAsyncNonQueryByIgnoreNull(String sqlQuery, List<Object> paramList, TTLEnumType ttl) throws Exception {
		init();
		if (!Strings.isNullOrEmpty(ttl.getValue()))
			sqlQuery += " USING TTL " + ttl.getValue();
		BoundStatement boundStatement = getBoundStatementByIgnoreNull(sqlQuery, paramList);
		boundStatement.setIdempotent(true);
		session.executeAsync(boundStatement);
	}

	/**
	 * [適用於INSERT]<br/>
	 * 批次查詢(非SELECT)(非同步)，設定為Idempotent=true且會忽略NULL值(不更新也不新增)<br/>
	 * PS. Idempotent=true。當連線斷開，會重試此命令，故需確認此資料不是更新語法
	 */
	public void executeBatchAsyncNonQueryByIgnoreNull(String sqlQuery, List<List<Object>> paramList, TTLEnumType ttl) throws Exception {
		init();
		if (!Strings.isNullOrEmpty(ttl.getValue()))
			sqlQuery += " USING TTL " + ttl.getValue();
		BatchStatement batchStatement = new BatchStatement();
		batchStatement.setIdempotent(true);
		for (List<Object> params : paramList)
			batchStatement.add(getBoundStatementByIgnoreNull(sqlQuery, params));
		session.executeAsync(batchStatement);
	}

	private BoundStatement getBoundStatementByIgnoreNull(String sqlQuery, List<Object> paramList) throws SQLException {
		BoundStatement boundStatement = null;
		PreparedStatement pStatement;
		synchronized (map) {
			pStatement = map.get(sqlQuery);
			if (pStatement == null) {
				pStatement = session.prepare(sqlQuery);
				map.put(sqlQuery, pStatement);
			}
		}
		boundStatement = pStatement.bind();

		int index = 0;
		for (Object sql_Parameter : paramList) {
			if (sql_Parameter != null) {
				Class<? extends Object> type = sql_Parameter.getClass();
				if (type == Integer.class)
					boundStatement.setInt(index, Integer.parseInt(sql_Parameter.toString()));
				else if (type == String.class)
					boundStatement.setString(index, sql_Parameter.toString());
				else if (type == Double.class)
					boundStatement.setDouble(index, Double.parseDouble(sql_Parameter.toString()));
				else if (type == Float.class)
					boundStatement.setFloat(index, Float.parseFloat(sql_Parameter.toString()));
				else if (type == BigDecimal.class)
					boundStatement.setDecimal(index, (BigDecimal) sql_Parameter);
				else if (type == Boolean.class)
					boundStatement.setBool(index, Boolean.parseBoolean(sql_Parameter.toString()));
				else if (type == Date.class)
					boundStatement.setTimestamp(index, (Date) sql_Parameter);
				else if (List.class.isAssignableFrom(type))
					boundStatement.setList(index, (List<?>) sql_Parameter);
				else if (Map.class.isAssignableFrom(type))
					boundStatement.setMap(index, (Map<?, ?>) sql_Parameter);
				else if (Set.class.isAssignableFrom(type))
					boundStatement.setSet(index, (Set<?>) sql_Parameter);
				else if (type == UDTValue.class)
					boundStatement.setUDTValue(index, (UDTValue) sql_Parameter);
				else if (type == TupleValue.class)
					boundStatement.setTupleValue(index, (TupleValue) sql_Parameter);
				else if (ByteBuffer.class.isAssignableFrom(type))
					boundStatement.setBytes(index, (ByteBuffer) sql_Parameter);
			}
			index++;
		}
		return boundStatement;
	}

	private class EupOnlineAddressTranslater implements AddressTranslator {

		private Map<InetSocketAddress, InetSocketAddress> privatePublicAddressMap = new HashMap<>();

		public EupOnlineAddressTranslater(String clusterName) {
			privatePublicAddressMap = CassandraIp_Mapping_Info.getInstance().getCassandraIpMapping(clusterName);
		}

		@Override
		public void close() {
		}

		@Override
		public void init(Cluster arg0) {
		}

		@Override
		public InetSocketAddress translate(InetSocketAddress arg0) {
			return privatePublicAddressMap.get(arg0);
		}
	}

	public void dispose() {
		session.close();
		cluster.close();
		session = null;
		cluster = null;
	}

	public String getDbName() {
		return dbName;
	}

}