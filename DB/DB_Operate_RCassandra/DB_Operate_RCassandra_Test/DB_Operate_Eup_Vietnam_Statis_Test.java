package DB_Operate_RCassandra.DB_Operate_RCassandra_Test;

import DB_Operate_RCassandra.DB_Operate_RCassandra;

public class DB_Operate_Eup_Vietnam_Statis_Test extends DB_Operate_RCassandra {

	private final String useName = "eup_vietnam_statis_test";
	private static DB_Operate_Eup_Vietnam_Statis_Test db_Operate_Eup_Statis_Test = null;
	private static final Object syncRoot = new Object();

	private DB_Operate_Eup_Vietnam_Statis_Test() throws Exception {
		super();
	}

	public static DB_Operate_Eup_Vietnam_Statis_Test getInstance() throws Exception {
		if (db_Operate_Eup_Statis_Test == null) {
			synchronized (syncRoot) {
				if (db_Operate_Eup_Statis_Test == null) {
					db_Operate_Eup_Statis_Test = new DB_Operate_Eup_Vietnam_Statis_Test();
				}
			}
		}
		return db_Operate_Eup_Statis_Test;
	}

	@Override
	protected String getUseName() {
		return useName;
	}

}
