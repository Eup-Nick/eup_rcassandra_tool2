package DB_Operate_RCassandra.DB_Operate_RCassandra_Test;

import DB_Operate_RCassandra.DB_Operate_RCassandra;

public class DB_Operate_Eup_Malaysia_Log_Test extends DB_Operate_RCassandra {

	private final String useName = "eup_malaysia_log_test";
	private static DB_Operate_Eup_Malaysia_Log_Test db_Operate_Eup_Log_Test = null;
	private static final Object syncRoot = new Object();

	private DB_Operate_Eup_Malaysia_Log_Test() throws Exception {
		super();
	}

	public static DB_Operate_Eup_Malaysia_Log_Test getInstance() throws Exception {
		if (db_Operate_Eup_Log_Test == null) {
			synchronized (syncRoot) {
				if (db_Operate_Eup_Log_Test == null) {
					db_Operate_Eup_Log_Test = new DB_Operate_Eup_Malaysia_Log_Test();
				}
			}
		}
		return db_Operate_Eup_Log_Test;
	}

	@Override
	protected String getUseName() {
		return useName;
	}

}
