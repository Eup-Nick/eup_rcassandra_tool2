package DB_Operate_RCassandra.DB_Operate_RCassandra_Online;

import DB_Operate_RCassandra.DB_Operate_RCassandra;

public class DB_Operate_Eup_Vietnam_Statis_Online extends DB_Operate_RCassandra {

	private final String useName = "eup_vietnam_statis_online";
	private static DB_Operate_Eup_Vietnam_Statis_Online db_Operate_Eup_Statis_Online = null;
	private static final Object syncRoot = new Object();

	private DB_Operate_Eup_Vietnam_Statis_Online() throws Exception {
		super();
	}

	public static DB_Operate_Eup_Vietnam_Statis_Online getInstance() throws Exception {
		if (db_Operate_Eup_Statis_Online == null) {
			synchronized (syncRoot) {
				if (db_Operate_Eup_Statis_Online == null) {
					db_Operate_Eup_Statis_Online = new DB_Operate_Eup_Vietnam_Statis_Online();
				}
			}
		}
		return db_Operate_Eup_Statis_Online;
	}

	@Override
	protected String getUseName() {
		return useName;
	}

}
