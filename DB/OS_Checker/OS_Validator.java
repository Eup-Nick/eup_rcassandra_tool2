package OS_Checker;

public class OS_Validator {

	static String os = System.getProperty("os.name").toLowerCase();

	static String linuxDesktop = System.getenv("DISPLAY");

	/**
	 * 取得OS版本
	 * 
	 * @return
	 */
	public static OS_Type getOSType() {
		OS_Type type = OS_Type.UnKnow;

		if (os.indexOf("win") >= 0)
			type = OS_Type.Windows;
		else if (os.indexOf("mac") >= 0)
			type = OS_Type.Mac;
		else if (os.indexOf("nix") >= 0 || os.indexOf("nux") >= 0)
			type = OS_Type.Linux;
		return type;
	}

	public static boolean isLinux() {
		return getOSType().equals(OS_Type.Linux);
	}

	/**
	 * 是否為linux的桌面
	 */
	public static boolean isLinuxDesktop() {
		return linuxDesktop != null;
	}
}
