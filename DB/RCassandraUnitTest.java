import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import DB_Operate_RCassandra.DB_Operate_RCassandra_Test.DB_Operate_Eup_Taiwan_Log_Test;
import DB_Operate_RRedis.DB_Operate_RRedis_Test.DB_Operate_Eup_Taiwan_RealTimeData_Test;

public class RCassandraUnitTest {

	@Test
	public void test() throws Exception {
		String sqlString = "SELECT dtime from logq_basic where unicode='79194' and pk='2020-08-18'";

		Map<String, List<Object>> map = DB_Operate_Eup_Taiwan_Log_Test.getInstance().executeQuery(sqlString, new ArrayList<Object>(), 1, 1);
		for (Map.Entry<String, List<Object>> kv : map.entrySet()) {
			for (Object o : kv.getValue()) {
				System.out.println(o.getClass());
			}
		}
	}

	@Test
	public void test1() throws Exception {
		String map = DB_Operate_Eup_Taiwan_RealTimeData_Test.getInstance().hget("dvrcmd:unicode:108099:INIT_REPORT", "recv_str");
		System.out.println(map);
	}
}
