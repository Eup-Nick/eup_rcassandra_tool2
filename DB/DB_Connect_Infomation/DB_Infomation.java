package DB_Connect_Infomation;

public class DB_Infomation {

	public String useName;
	public String dbName;
	public String ipAddress;
	public String port;
	public String loginID;
	public String password;
	public String clusterName;

	public String uri;
	public String masterKey;

	public DB_Infomation(String useName, String dbName, String ipAddress, String port, String loginID, String password, String clusterName) {
		this.useName = useName;
		this.dbName = dbName;
		this.ipAddress = ipAddress;
		this.port = port;
		this.loginID = loginID;
		this.password = password;
		this.clusterName = clusterName;
	}

	public DB_Infomation(String useName, String dbName, String uri, String masterKey) {
		this.useName = useName;
		this.dbName = dbName;
		this.uri = uri;
		this.masterKey = masterKey;
	}
}
