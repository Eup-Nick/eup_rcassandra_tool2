package DB_Connect_Infomation;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import OS_Checker.OS_Type;
import OS_Checker.OS_Validator;

public class DB_Connect_Info {

	public static final String LIB_DB_FILE_NAME = "Lib_DB.XML";

	private static DB_Connect_Info connect_Info;
	private static final Object sync = new Object();
	private int I18N = 0;
	private String nationCode = "", onlinelogdata_DBType = "MS";
	private boolean onlinelogdata = false;
	private Map<String, DB_Infomation> dbConnectionInfo = new HashMap<String, DB_Infomation>();
	private Map<String, String> blobConnectionInfo = new HashMap<String, String>();

	public static DB_Connect_Info getInstance() {
		if (connect_Info == null)
			synchronized (sync) {
				if (connect_Info == null)
					connect_Info = new DB_Connect_Info();
			}

		return connect_Info;
	}

	private DB_Connect_Info() {
		readXML();
	}

	private void readXML() {
		try {
			File fXmlFile = null;
			if (OS_Validator.getOSType() == OS_Type.Linux)
				fXmlFile = new File("/opt/tomcat/conf/" + LIB_DB_FILE_NAME);
			else
				fXmlFile = new File("C:\\Config\\" + LIB_DB_FILE_NAME);

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(fXmlFile);
			readNational(doc);
			readOnlineLog(doc);
			readAzureBlobStorage(doc);

			NodeList nList = doc.getElementsByTagName("dtDB");
			for (int i = 0; i < nList.getLength(); i++) {
				Node nNode = nList.item(i);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;
					String useName = eElement.getElementsByTagName("UseName").item(0).getTextContent();
					String dbName = eElement.getElementsByTagName("DbName").item(0).getTextContent();
					String ipAddress = eElement.getElementsByTagName("IPAddress").item(0).getTextContent();
					String port = eElement.getElementsByTagName("Port").item(0).getTextContent();
					String id = eElement.getElementsByTagName("ID").item(0).getTextContent();
					String pw = eElement.getElementsByTagName("PW").item(0).getTextContent();
					String clusterName = eElement.getElementsByTagName("ClusterName").item(0) == null ? null
							: eElement.getElementsByTagName("ClusterName").item(0).getTextContent();
					DB_Infomation db_Info = new DB_Infomation(useName, dbName, ipAddress, port, id, pw, clusterName);
					if (db_Info != null && dbConnectionInfo.containsKey(useName) == false)
						dbConnectionInfo.put(useName, db_Info);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void readNational(Document doc) {
		NodeList iNational = doc.getElementsByTagName("iNat");
		if (iNational.getLength() > 0) { // 取得國際版本
			Element eElement = (Element) iNational.item(0);
			I18N = Integer.parseInt(eElement.getElementsByTagName("NationVersion").item(0).getTextContent());
			nationCode = eElement.getElementsByTagName("NationCode").getLength() > 0
					? eElement.getElementsByTagName("NationCode").item(0).getTextContent() : "";
		}

		if (I18N == 1)
			TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	private void readOnlineLog(Document doc) {
		NodeList onlineLogData = doc.getElementsByTagName("OnlineLogData");
		if (onlineLogData.getLength() > 0) {
			Element eElement = (Element) onlineLogData.item(0);
			onlinelogdata = Boolean.parseBoolean(eElement.getElementsByTagName("UseOnlineData").item(0).getTextContent());
			onlinelogdata_DBType = eElement.getElementsByTagName("DB_Type").item(0).getTextContent();
		}
	}

	private void readAzureBlobStorage(Document doc) {
		NodeList nodeList = doc.getElementsByTagName("AzureBlobStorage");
		for (int i = 0; i < nodeList.getLength(); i++) {
			Element eElement = (Element) nodeList.item(i);
			String useName = eElement.getElementsByTagName("UseName").item(0).getTextContent();
			String conntr = eElement.getElementsByTagName("Conn_Str").item(0).getTextContent();
			blobConnectionInfo.put(useName, conntr);
		}
	}

	public boolean isOnlinelogdata() {
		return onlinelogdata;
	}

	public String getOnlinelogdata_DBType() {
		return onlinelogdata_DBType;
	}

	public DB_Infomation getDBInfo(String dbName) throws Exception {
		if (dbConnectionInfo.containsKey(dbName) == true) {
			return dbConnectionInfo.get(dbName);
		} else {
			throw new Exception("無資料庫連線資訊，請檢查XML是否正確。(DBNAME:" + dbName + ")");
		}
	}

	public boolean isNat() {
		return I18N == 1;
	}

	public String getNationCode() {
		return nationCode;
	}

	public String getBlobConnStr(String blobName) {
		return blobConnectionInfo.get(blobName);
	}
}
