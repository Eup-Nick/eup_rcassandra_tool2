package DB_Operate_Object;

public enum TTLEnumType {
	// ����쬰��
	FOREVER(""), ThreeYears("94608000"), TwoYears("63072000"), OneYear("31536000"), SixMonths("16070400"), ThreeMonths("8035200"), TwoMonths(
			"5356800"), OneMonth("2678400"), TwoWeeks("1209600"), OneWeek("604800"), ThreeDays("259200"), OneDay("86400"), OneMinute("60"); // FortyDays("3456000")

	private String value = "";

	private TTLEnumType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}

	public int getIntValue() {
		if (this.value.isEmpty())
			return 0;
		return Integer.valueOf(this.value);
	}
}
